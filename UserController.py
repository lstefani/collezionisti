import random
import string
from flask import Flask, render_template, redirect, request, flash, url_for, session, Blueprint
from flask_login import LoginManager, login_user, current_user
from flask_session import Session
from dbclasses import *

usercontroller = Blueprint('usercontroller', __name__, template_folder='templates')






# METODI

@usercontroller.route('/register/createuser', methods = ['GET', 'POST'])
def createuser():
    user = User(
        email=request.form.get('email')
    )
    collezionista = Collezionista(
        nickname=request.form.get('name'),
        via=request.form.get('location'),
        city=request.form.get('city'),
        avatar_id=request.form.get('avatar_id'),
        cap=request.form.get('cap'),
        bio=request.form.get('bio')
    )

    user.set_password(request.form.get('password'))

    db.session.add(user)
    db.session.commit()

    collezionista.co_id = collezionista.id
    collezionista.userid = user.id
    db.session.add(collezionista)
    db.session.commit()

    flash("Registrazione completata, accedi con le tue credenziali per continuare")
    return redirect('/login')