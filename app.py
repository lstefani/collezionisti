import os
import random
import string
from flask import Flask, render_template, redirect, request, flash, url_for, session
from flask_login import LoginManager, login_user, current_user
from flask_session import Session
from dbclasses import *
from datetime import datetime, timedelta
from flask_login import logout_user


app = Flask(__name__)
letters = string.ascii_lowercase

UPLOAD_FOLDER = 'static'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app.config['SESSION_TYPE'] = 'filesystem'
app.config['SECRET_KEY'] = ''.join(random.choice(letters) for i in range(10))
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://collezionisti:collezionisti@localhost/collezionisti'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config.from_object(__name__)
db.init_app(app)

with app.app_context():
    db.create_all()
Session(app)

login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


@app.route('/')
def index():
    return redirect('/home')


@app.route('/home', methods=['GET', 'POST'])
def home():
    users = []
    session['user2'] = None

    f = request.form.get('filtro')

    if request.form.get('delete'):
        f = None

    if not request.form.get('carte'):
        users = Collezionista.query.all()

    if request.form.get('carte') and request.form.get('carte') == "0":
        if f:
            users = Collezionista.query.filter_by(nickname=request.form.get('filtro'))
        elif not f:
            users = Collezionista.query.filter_by()

    elif request.form.get('carte') and request.form.get('carte') == "1":
        if f:
            utenti = Collezionista.query.all()
            for user in utenti:
                for carta in user.carte:
                    if carta.name == f:
                        users.append(user)

        elif not f:
            users = Collezionista.query.all()

    Ac = Collezionista.query.filter_by(id=current_user.get_id()).first()
    return render_template('home.html', users=users, Ac=Ac, filtervalue=f)


@app.route('/changes', methods=['GET', 'POST'])
def exchanges():
    f = request.form.get('filtro')
    scambi = Scambio.query.filter_by(id2=current_user.get_id())

    today = datetime.now().strftime("%Y-%m-%d")
    end = (datetime.now() + timedelta(days=10)).strftime("%Y-%m-%d")

    return render_template('exchanges.html', scambi=scambi, date=today, date2=end)


@app.route('/sentchanges')
def sentexchanges():
    scambi = Scambio.query.filter_by(id1= current_user.get_id())

    return render_template('sentexchanges.html', scambi=scambi)


@app.route('/myprofile', methods=['GET', 'POST'])
def myprofile():
    if current_user.get_id() is None:
            return redirect('/login')

    carte = []

    c = Collezionista.query.filter_by(id=current_user.get_id()).first()

    f = request.form.get('filtro')
    if f:
        for carta in c.carte:
            if carta.name == f:
                carte.append(carta)
    else:
        carte = c.carte

    return render_template('myprofile.html', user=current_user, profile=c, carte=carte)


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/login', methods=['POST', 'GET'])
def login():
    return render_template('Login.html')


@app.route('/register', methods=['POST', 'GET'])
def register():
    return render_template('Register.html')


@app.route('/cardsadd')
def cardsadd():
    if current_user.get_id() is None:
        return redirect('/login')

    carte = Carta.query.all()
    c = Collezionista.query.filter_by(id=current_user.get_id()).first()
    return render_template('cardsadd.html', carte=carte, profile=c)


@app.route('/searchedadd')
def searchedadd():
    if current_user.get_id() is None:
        return redirect('/login')

    carte = Carta.query.all()
    c = Collezionista.query.filter_by(id=current_user.get_id()).first()
    return render_template('searchedadd.html', carte=carte, profile=c)


@app.route('/reportuser/<ids>')
def reportuser(ids):
    Rprofile = Collezionista.query.filter_by(id = current_user.get_id())
    return render_template('Report.html', profile=Rprofile, id2=ids)


@app.route('/admin', methods = ['GET', 'POST'])
def admin():
    if current_user.get_id() is None:
        return redirect('/login')

    if current_user.role != "admin":
        flash('la pagina selezionata è indicata per gli amministratori, accesso non consentito', 'error')
        return redirect('/home')

    if request.form.get('filtro') and request.form.get('delete') != "1":
        c = Collezionista.query.filter_by(nickname=request.form.get('filtro'))
        for collezionista in c:
            users = User.query.filter_by(id=collezionista.id)
    else:
        users = User.query.all()

    return render_template('admin.html', users=users, filtervalue=request.form.get('filtro'))


@app.route('/admin/cards', methods=['GET', 'POST'])
def admincards():
    if current_user.get_id() is None:
        return redirect('/login')

    if current_user.role != "admin":
        flash('la pagina selezionata è indicata per gli amministratori, accesso non consentito', 'error')
        return redirect('/home')

    if request.form.get('filtro') and request.form.get('delete') != "1":
        carte = Carta.query.filter_by(name=request.form.get('filtro'))
    else:
        carte = Carta.query.all()

    return render_template('admincards.html', carte=carte, filtervalue=request.form.get('filtro'))


@app.route('/admin/report/<id>')
def singlereport(id):
    if current_user.get_id() is None:
        return redirect('/login')

    if current_user.role != "admin":
        flash('la pagina selezionata è indicata per gli amministratori, accesso non consentito', 'error')
        return redirect('/home')

    col = Collezionista.query.filter_by(id=id)
    report = Report.query.filter_by(to=id)

    return render_template('singlereport.html', col = col, report = report)


@app.route('/admin/cards/add')
def admincardsadd():
    if current_user.get_id() is None:
        return redirect('/login')

    return render_template('admincardsadd.html')


@app.route('/exchange/<int:iduser2>', methods=['GET', 'POST'])
def exchange(iduser2):

    if session['user2'] != iduser2:
        session['user2'] = iduser2
        session['pool1'] = []
        session['pool2'] = []

    col1 = Collezionista.query.filter_by(id=current_user.get_id())
    col2 = Collezionista.query.filter_by(id=iduser2)

    today = datetime.now().strftime("%Y-%m-%d")
    end = (datetime.now() + timedelta(days=10)).strftime("%Y-%m-%d")

    return render_template('change.html', col1=col1, col2=col2, pool1=session['pool1'], pool2=session['pool2'], date=today, date2=end)


@app.route('/pooladd/<cid>/<id2>', methods = ['GET', 'POST'])
def pooladd(cid, id2):
    if current_user.get_id() is None:
        return redirect('/login')

    c = Collezionista.query.filter_by(id=cid)

    return render_template('pooladd.html', profile=c, id2= id2)


@app.route('/editProfile')
def editProfile():
    if current_user.get_id() is None:
        return redirect('/login')

    c = Collezionista.query.filter_by(id=current_user.get_id()).first()
    return render_template('editProfile.html', profile=c)


@app.route('/profile/<id>')
def profile(id):
    if current_user.get_id() is None:
        return redirect('/login')

    session['pool1'] = None
    session['pool2'] = None

    c = Collezionista.query.filter_by(id=id).first()
    image = 'avatar ('+str(c.avatar_id)+').png'
    profilo = Collezionista.query.filter_by(id=current_user.get_id()).first()

    f = request.form.get('filtro')
    if f is None:
        carte = c.carte
    else:
        carte = Carta.query.filter_by(name=f)

    return render_template('profile.html', profile=c, img=str(image), profilo=profilo, carte=carte)


@app.route('/admin/reports')
def rep():
    return render_template('Report.html')

# METODI


@app.route('/register/createuser', methods = ['GET', 'POST'])
def createuser():

    email = request.form.get('email')
    nickname = request.form.get('name')
    via = request.form.get('location')
    city = request.form.get('city')
    avatar_id = request.form.get('avatar_id')
    cap = request.form.get('cap')
    bio = request.form.get('bio')

    print(email, nickname)

    if email == "" or nickname =="" or via is None or city is None or avatar_id is None or cap is None or bio is None:
        flash("Campi incompleti, riprovare", 'error')
        return redirect(request.referrer)

    user = User(
        email=request.form.get('email'),
    )

    collezionista = Collezionista(
        nickname=request.form.get('name'),
        via=request.form.get('location'),
        city=request.form.get('city'),
        avatar_id=request.form.get('avatar_id'),
        cap=request.form.get('cap'),
        bio=request.form.get('bio')
    )

    user.set_password(request.form.get('password'))

    db.session.add(user)
    db.session.commit()

    collezionista.co_id = collezionista.id
    collezionista.userid = user.id
    db.session.add(collezionista)
    db.session.commit()

    flash("Registrazione completata, accedi con le tue credenziali per continuare")
    return redirect('/login')


@app.route('/login/user', methods=['GET', 'POST'])
def loginuser():
    user = User.query.filter_by(email=request.form.get('email')).first()

    if user is not None and user.role == "admin":
        login_user(user)
        flash('Login effettuato con successo')
        return redirect('/admin')

    if user is not None and user.check_password(request.form.get('password')):
        login_user(user)
        flash('Login effettuato con successo')
        session['pool1'] = None
        session['pool2'] = None
        return redirect('/home')

    flash('Email o password non valida', 'error')
    return redirect('/login')


@app.route('/addcomment', methods=['GET', 'POST'])
def addcomment():
    comment = Commento(
        content=request.form.get('commentcontent'),
        to_id=request.form.get('userto'),
        co_id=request.form.get('userby')
    )
    db.session.add(comment)
    db.session.commit()

    return redirect(request.referrer)


@app.route('/addselection/<id2>/<idwho>', methods=['GET', 'POST'])
def addselection(id2, idwho):

    cards = request.form.getlist('card[]')

    if id2 == idwho:
        for i in range(len(cards)):
            c = Carta.query.filter_by(id=int(cards[i]))
            for carta in c:
                session['pool2'].append(carta)
    else:
        for i in range(len(cards)):
            c = Carta.query.filter_by(id=int(cards[i]))
            for carta in c:
                session['pool1'].append(carta)

    return redirect(url_for('exchange', iduser2=id2))


@app.route('/addchange', methods=['GET', 'POST'])
def addchange():

    if request.form.get('data1') == "":
        flash("Scambio non inviato, assicurati di inserire un messaggio e la data di ritiro", "error")
        return redirect(request.referrer)

    s = Scambio(
        id1=current_user.get_id(),
        id2=session['user2'],
        messaggio=request.form.get('messaggio'),
        stato='active',
        data1=request.form.get('data1')
    )
    db.session.add(s)
    db.session.commit()

    for carta in session['pool1']:
            n = Selezione(
                idscambio=s.id,
                cardid=carta.id,
                who=current_user.get_id()
            )
            db.session.add(n)
            db.session.commit()

    for carta in session['pool2']:
            n = Selezione(
                idscambio=s.id,
                cardid=carta.id,
                who=session['user2']
            )
            db.session.add(n)
            db.session.commit()

    session['user2'] = None
    flash("Scambio inviato correttamente")

    return redirect('/home')


@app.route('/changestatus', methods=['GET', 'POST'])
def changestatus():
    s = Scambio.query.filter_by(id=request.form.get('idscambio')).first()
    s.stato = request.form.get('status')
    if s.stato != 'refused':
        s.data2 = request.form.get('data2')
        if request.form.get('data2') == "":
            flash("Impossibile accettare lo scambio, assicurati di inserire una data valida.")
            return redirect(request.referrer)

    db.session.commit()

    return redirect(request.referrer)


@app.route('/deletechange', methods=['GET', 'POST'])
def deletechange():
    s = Scambio.query.filter_by(id=request.form.get('idscambio')).first()
    db.session.delete(s)
    db.session.commit()

    return redirect(request.referrer)


@app.route('/deletecard', methods=['GET', 'POST'])
def deletecard():
    cardid = request.form.get('cardid')
    userid = request.form.get('userid')
    where = request.form.get('where')

    if int(where) == 1:
        db.engine.execute("DELETE FROM collezioni WHERE idcarta="+cardid+" AND idcollezionista="+userid)
    else:
        db.engine.execute("DELETE FROM ricercate WHERE idcarta=" + cardid + " AND idcollezionista=" + userid)

    db.session.commit()

    return redirect('/myprofile')


@app.route('/deletecomm', methods = ['GET', 'POST'])
def deletecomm():
    id = request.form.get('id')
    db.engine.execute("DELETE FROM commenti WHERE id="+id)
    db.session.commit()

    return redirect('/myprofile')


@app.route('/addcard', methods = ['GET', 'POST'])
def addcard():
    cardid = request.form.get('cardid')
    type = request.form.get('type')
    collid = request.form.get('userid')

    col = Collezionista.query.filter_by(id=collid).first()

    if int(type) == 1:
        for carta in col.carte:
            if carta.id == int(cardid):
                flash('carta già inserita nella collezione', 'error')
                return redirect("/myprofile")
        db.engine.execute("INSERT INTO collezioni VALUES("+collid+", "+cardid+")")
        db.session.commit()
    else:
        for carta in col.ricercate:
            if carta.id == cardid:
                flash('carta già inserita nelle ricercate', 'error')
                return redirect(request.referrer)
        db.engine.execute("INSERT INTO ricercate VALUES("+collid+", "+ cardid +")")
        db.session.commit()

    return redirect('/myprofile');


@app.route('/upuser', methods=['POST', 'GET'])
def edituser():
    c = Collezionista.query.filter_by(id=current_user.get_id()).first()
    c.nickname = request.form.get('name')
    c.via = request.form.get('indirizzo')
    c.city = request.form.get('city')
    c.cap = request.form.get('cap')

    if request.form.get('bio') is not None:
        c.bio = request.form.get('bio')

    if request.form.get('avatar_id') is not None:
        c.avatar_id = request.form.get('avatar_id')

    current_user.email = request.form.get('email')

    if request.form.get('password') is not None:
        current_user.set_password(request.form.get('password'))

    db.session.merge(c)
    db.session.merge(current_user)

    db.session.commit()

    return redirect('/myprofile')


@app.route('/reportadd', methods=['POST', 'GET'])
def reportadd():

    r = Report(to=request.form.get('idtoreport'), by=current_user.get_id(), motive=request.form.get('motivetxt'))
    db.session.add(r)
    db.session.commit()
    flash('Utente segnalato')

    return redirect(url_for('profile', id=request.form.get('idtoreport')))


@app.route('/banuser', methods=['POST', 'GET'])
def banuser():

    id2 = request.form.get('idtoban')
    print(id2)
    u = User.query.filter_by(id=id2).first()
    db.session.delete(u)
    db.session.commit()
    flash('Utente bannato')

    return redirect('/admin')


@app.route('/addcards', methods=['GET', 'POST'])
def addcards():
    f = request.files['file']
    if f and request.form.get('nome'):
        c = Carta(
           name=request.form.get('nome'),
        )
        c.image = c.name + ".png"
        f.save(os.path.join(UPLOAD_FOLDER, c.name+".png"))
        db.session.add(c)
        db.session.commit()
        flash('Carta inserita correttamente')

    else:
        flash('Carta non inserita, errore.')
    return redirect(request.referrer)


@app.route('/deletecards', methods = ['GET', 'POST'])
def deletecards():
    c = Carta.query.filter_by(id=request.form.get('idcarta')).first()
    db.session.delete(c)
    db.session.commit()
    return redirect(request.referrer)


@app.route("/logout")
def logout():
    logout_user()
    return redirect("/login")


if __name__ == "__main__":
    app.run(debug=True)