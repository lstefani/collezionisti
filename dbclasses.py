from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(50))
    password_hash = db.Column(db.String(255))
    profile = db.relationship("Collezionista", cascade="all, delete-orphan")
    role = db.Column(db.String(10))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


ricercate = db.Table('ricercate',
    db.Column('idcollezionista', db.ForeignKey('collezionisti.id')),
    db.Column('idcarta', db.ForeignKey('carte.id')))


collezioni = db.Table('collezioni',
    db.Column('idcollezionista', db.ForeignKey('collezionisti.id')),
    db.Column('idcarta', db.ForeignKey('carte.id')))




class Collezionista(db.Model):
    __tablename__ = 'collezionisti'
    userid = db.Column(db.Integer, db.ForeignKey('users.id'))
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nickname = db.Column(db.String(50))
    via = db.Column(db.String(255))
    city = db.Column(db.String(255))
    avatar_id = db.Column(db.String(1))
    cap = db.Column(db.String(50))
    bio = db.Column(db.Text(700))
    carte = db.relationship("Carta", secondary= 'collezioni')
    ricercate = db.relationship("Carta", secondary='ricercate')
    Cowner = db.relationship("Commento", backref="writer",  cascade="all, delete-orphan", foreign_keys='Commento.co_id')
    SelectedIn = db.relationship("Scambio",  backref="selezionato",  cascade="all, delete-orphan", foreign_keys='Scambio.id2')
    Ssender = db.relationship("Scambio", backref="sender",  cascade="all, delete-orphan", foreign_keys='Scambio.id1')
    reports = db.relationship("Report", backref="subject",  cascade="all, delete-orphan", foreign_keys='Report.to')
    repby = db.relationship("Report", backref="from",  cascade="all, delete-orphan", foreign_keys='Report.by')


class Commento(db.Model):
    __tablename__ = 'commenti'
    id = db.Column(db.Integer, primary_key=True,  autoincrement=True)
    to_id = db.Column(db.Integer, db.ForeignKey(Collezionista.id))
    co_id = db.Column(db.Integer, db.ForeignKey(Collezionista.id))
    to = db.relationship("Collezionista", backref="commenti", foreign_keys=[to_id])
    content = db.Column(db.Text(200))



class Admin(db.Model):
    __tablename__ = 'admins'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nickname = db.Column(db.String(50))
    grade = db.Column(db.Integer)

    def __init__ (self, id, nickname, grade):
        self.id = id
        self.nickname = nickname
        self.grade = grade


class Scambio(db.Model):
    __tablename__ = 'scambi'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    id1 = db.Column(db.Integer, db.ForeignKey(Collezionista.id))
    id2 = db.Column(db.Integer, db.ForeignKey(Collezionista.id))
    data1 = db.Column(db.String(20))
    data2 = db.Column(db.String(20))
    messaggio = db.Column(db.Text(500))
    selezioni = db.relationship("Selezione", cascade="all, delete-orphan")
    stato = db.Column(db.String(20))

class Carta(db.Model):
    __tablename__ = 'carte'
    id = db.Column(db.Integer,  primary_key=True, autoincrement = True)
    image = db.Column(db.String(255))
    name = db.Column(db.String(50))
    owners = db.relationship('Collezionista',  secondary='collezioni')
    wantedby = db.relationship('Collezionista', secondary='ricercate')
    Cowner = db.relationship("Selezione", cascade="all, delete-orphan", backref="card", foreign_keys='Selezione.cardid')



class Selezione(db.Model):
    __tablename__ = 'selezioni'
    id = db.Column(db.Integer, primary_key=True,  autoincrement=True)
    idscambio = db.Column(db.Integer, db.ForeignKey(Scambio.id))
    cardid = db.Column(db.Integer, db.ForeignKey(Carta.id))
    who = db.Column(db.Integer, db.ForeignKey(Collezionista.id))


class Follow(db.Model):
    __tablename__ = 'follows'
    by = db.Column(db.Integer, primary_key=True)
    to = db.Column(db.Integer)

    def __init__(self, by, to):
        self.by = by
        self.to = to


class Report(db.Model):
    __tablename__ = 'reports'
    id = db.Column(db.Integer, primary_key=True,  autoincrement=True)
    by = db.Column(db.Integer, db.ForeignKey(Collezionista.id))
    to = db.Column(db.Integer, db.ForeignKey(Collezionista.id))
    motive = db.Column(db.Text(255))





